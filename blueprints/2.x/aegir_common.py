#! /usr/bin/env python

"""Common functions for the Aegir tesing scripts
"""

import fabric.api as fabric
import os, sys, glob

# Fabric command to set some Apache requirements
def fab_prepare_apache():
        print "===> Preparing Apache"
        fabric.sudo("a2enmod rewrite", pty=True)
        fabric.sudo("ln -s /var/aegir/config/apache.conf /etc/apache2/conf.d/aegir.conf", pty=True)

# Fabric command to add the aegir user and to sudoers also
def fab_prepare_user():
        print "===> Preparing the Aegir user"
        fabric.sudo("useradd -r -U -d /var/aegir -m -G www-data aegir", pty=True)
        fabric.sudo("echo 'aegir ALL=NOPASSWD: /usr/sbin/apache2ctl' >> /etc/sudoers", pty=True)

# Fabric command to fetch Drush
def fab_fetch_drush(drush_version):
        print "===> Fetching Drush %s" % (drush_version)
        fabric.sudo("su - -s /bin/sh aegir -c 'wget http://ftp.drupal.org/files/projects/drush-%s.tar.gz'" % (drush_version), pty=True)
        fabric.sudo("su - -s /bin/sh aegir -c 'gunzip -c drush-%s.tar.gz | tar -xf - '" % (drush_version), pty=True)
        fabric.sudo("su - -s /bin/sh aegir -c 'rm /var/aegir/drush-%s.tar.gz'" % (drush_version), pty=True)
        fabric.sudo("chmod u+x /var/aegir/drush/drush", pty=True)
        # Add a symlink for Drush.
        fabric.sudo("ln -s /var/aegir/drush/drush /usr/bin/drush", pty=True)

# Force the dispatcher
def fab_run_dispatch():
        fabric.sudo("su - -s /bin/sh aegir -c 'php /var/aegir/drush/drush.php @hostmaster hosting-dispatch'", pty=True)

# Fabric command to add the apt sources
def fab_add_apt_sources(distro = 'unstable', drush_distro = 'ppa'):
        print "===> Adding apt sources"
        # Add the apt-key for Aegir.
        fabric.sudo("curl http://debian.aegirproject.org/key.asc | apt-key add -", pty=True)
        # Add the unstable Aegir repositories, these should contain the dev version of Aegir.
        fabric.sudo("echo 'deb http://debian.aegirproject.org %s main' >> /etc/apt/sources.list" % distro, pty=True)
        if (drush_distro == 'ppa'):
            # Add the ppa repo for Drush.
            fabric.sudo("apt-get install python-software-properties -y", pty=True)
            fabric.sudo("add-apt-repository ppa:brianmercer/drush", pty=True)
        else:
            fabric.sudo("echo 'APT::Cache-Limit 50000000;' > /etc/apt/apt.conf.d/10aegir", pty=True)
            fabric.sudo("echo 'deb http://archive.ubuntu.com/ubuntu/ %s universe' >> /etc/apt/sources.list" % drush_distro, pty=True)
        fabric.sudo("apt-get update", pty=True)

# Fabric command to preseed Aegir.
def fab_preseed_config(domain, email, mysqlpass):
        print "===> Preseeding Aegir"
        # Preseed the options for the aegir package.
        fabric.sudo("apt-get install debconf-utils -y", pty=True)
        fabric.sudo("echo 'aegir-hostmaster aegir/db_password password %s' | debconf-set-selections" % (mysqlpass), pty=True)
        fabric.sudo("echo 'aegir-hostmaster aegir/db_password seen true' | debconf-set-selections", pty=True)
        fabric.sudo("echo 'aegir-hostmaster aegir/db_host string localhost' | debconf-set-selections", pty=True)
        fabric.sudo("echo 'aegir-hostmaster aegir/email string %s' | debconf-set-selections" % (email), pty=True)
        fabric.sudo("echo 'aegir-hostmaster aegir/site string %s' | debconf-set-selections" % (domain), pty=True)

# Fabric command to install Aegir using apt_get
def fab_apt_install_aegir():
        print "===> Installing Aegir"
        # Install aegir, but ensure that no questions are prompted.
        fabric.sudo("DPKG_DEBUG=developer DEBIAN_FRONTEND=noninteractive apt-get install aegir -y", pty=True)

# Install Aegir from debs.
def fab_install_debs(debs, drush_distro = 'ppa'):
        for deb in glob.glob(debs):
                print "===> Uploading deb " + deb
                fabric.put(deb, '~')
        print "===> Installing Drush"
        if (drush_distro == 'ppa'):
            fabric.sudo("apt-get install drush -y", pty=True)
        else:
            fabric.sudo("apt-get install -t %s drush -y" % drush_distro, pty=True)
        print "===> Installing Drush Make"
        fabric.sudo("apt-get install drush-make -y", pty=True)
        print "===> Installing Aegir"
        fabric.sudo("DPKG_DEBUG=developer DEBIAN_FRONTEND=noninteractive dpkg -i ~/aegir*.deb || apt-get install", pty=True)
        # Install aegir, but ensure that no questions are prompted.
        #fabric.run("DPKG_DEBUG=developer DEBIAN_FRONTEND=noninteractive apt-get install aegir -y", pty=True)

# Remove and purge the aegir debian install
def fab_apt_uninstall_aegir():
        fabric.sudo("apt-get remove --purge aegir aegir-hostmaster aegir-provision drush -y", pty=True)


def fab_run_provision_tests():
        print "===> Running Provision tests"
        fabric.sudo("su - -s /bin/sh aegir -c 'drush cache-clear drush -y || true'", pty=True)
        fabric.sudo("su - -s /bin/sh aegir -c 'drush @hostmaster provision-tests-run -y'", pty=True)

        # Fabric command to fetch Provision
def fab_fetch_provision(release_type, aegir_version):
        if release_type == "git":
                print "===> Fetching Provision - via git"
                fabric.sudo("su - -s /bin/sh aegir -c 'mkdir ~/.drush'", pty=True)
                fabric.sudo("su - -s /bin/sh aegir -c 'git clone http://git.drupal.org/project/provision.git ~/.drush/provision'", pty=True)
                fabric.sudo("su - -s /bin/sh aegir -c 'cd ~/.drush/provision && git checkout %s'" % (aegir_version), pty=True)
        else:
                print "===> Fetching Provision - via package"
                fabric.sudo("su - -s /bin/sh aegir -c 'php /var/aegir/drush/drush.php dl -y --destination=/var/aegir/.drush provision-%s'" % (aegir_version), pty=True)
                
# Fabric command to fetch Provision
def fab_link_provision(location = 'provision'):
                fabric.sudo("su - -s /bin/sh aegir -c 'mkdir ~/.drush'", pty=True)
                fabric.sudo("su - -s /bin/sh aegir -c 'ln -s /vagrant/provision/ ~/.drush/provision'", pty=True)

# Fabric command to run the install.sh aegir script
def fab_hostmaster_install(domain, email, mysqlpass):
        print "===> Running hostmaster-install"
        #fabric.sudo("su - -s /bin/sh aegir -c 'php /var/aegir/drush/drush.php hostmaster-install \"%s\" --client_email=\"%s\" --aegir_db_pass=\"%s\" --yes --debug'" % (domain, email, mysqlpass), pty=False)
        #fabric.sudo("su - -s /bin/sh aegir -c 'php /var/aegir/drush/drush.php -y @hostmaster vset hosting_queue_tasks_frequency 1'", pty=True)
        fab_run_dispatch()

# Fabric command to run the install.sh aegir script
def fab_hostmaster_local_install(domain, email, mysqlpass):
        print "===> Building the hostmaster platform"
        fabric.sudo("su - -s /bin/sh aegir -c 'drush make ~/.drush/provision/aegir.make /var/aegir/platforms/hostmaster-dev --projects=drupal'", pty=False)
        
        print "===> Linking in the hostmaster profile"
        fabric.sudo("su - -s /bin/sh aegir -c 'rm -rf ~/platforms/hostmaster-dev/profiles/hostmaster'", pty=False)
        fabric.sudo("su - -s /bin/sh aegir -c 'ln -s /vagrant/hostmaster/ ~/platforms/hostmaster-dev/profiles/hostmaster'", pty=False)
        
        print "===> Running hostmaster-install"
        
        fabric.sudo("su - -s /bin/sh aegir -c 'php /var/aegir/drush/drush.php hostmaster-install \"%s\" --client_email=\"%s\" --aegir_db_pass=\"%s\" --root=\"/var/aegir/platforms/hostmaster-dev\" --yes'" % (domain, email, mysqlpass), pty=False)
        fabric.sudo("su - -s /bin/sh aegir -c 'php /var/aegir/drush/drush.php -y @hostmaster vset hosting_queue_tasks_frequency 1'", pty=True)
        fab_run_dispatch()

from fabric.api import env, local, run

from aegir_common import fab_prepare_apache, fab_prepare_user, fab_fetch_drush, fab_fetch_provision, fab_hostmaster_install, fab_run_provision_tests, fab_add_apt_sources, fab_apt_install_aegir, fab_apt_uninstall_aegir, fab_preseed_config, fab_install_debs, fab_link_provision, fab_hostmaster_local_install

import os
import glob

def vagrant():
    # Lookup the user name for ssh.
    result = local('vagrant ssh-config | grep User', capture=True)
    env.user = result.split()[1]

    # Lookup the port and hostname.
    result = local('vagrant ssh-config | grep HostName', capture=True)
    hostname = result.split()[1]
    result = local('vagrant ssh-config | grep Port', capture=True)
    port = result.split()[1]

    # connect to the port-forwarded ssh
    env.hosts = [hostname + ':' + port]

    # use vagrant ssh key
    result = local('vagrant ssh-config | grep IdentityFile', capture=True)
    env.key_filename = result.split()[1]

def aegir_manual_install(release_type = 'git', aegir_version = '6.x-1.x', drush_version = '7.x-4.5'):
    fab_prepare_apache()
    fab_prepare_user()
    fab_fetch_drush(drush_version)
    fab_fetch_provision('git', aegir_version)
    # Not sure if we need to make these configurable.
    domain = 'example.some.domain'
    email = 'example@example.com'
    # This gets set in the puppet config in /modules/mysql/files/mysql-server.preseed
    mysqlpass = 'root'
    fab_hostmaster_install(domain, email, mysqlpass)
    fab_run_provision_tests()

def aegir_apt_install(distro = 'unstable'):
    fab_prepare_user()
    fab_add_apt_sources(distro)

    # Not sure if we need to make these configurable.
    domain = 'example.some.domain'
    email = 'example@example.com'
    # This gets set in the puppet config in /modules/mysql/files/mysql-server.preseed
    mysqlpass = 'root'
    fab_preseed_config(domain, email, mysqlpass)
    fab_apt_install_aegir()
    fab_run_provision_tests()
    fab_apt_uninstall_aegir()

def aegir_dpkg_install(debs, distro = 'unstable', drush_distro = 'quantal'):
    for deb in glob.glob(debs):
            assert(os.path.exists(deb)), "provided file doesn't exist: " + deb
    fab_prepare_user()
    fab_add_apt_sources(distro, drush_distro)

    # Not sure if we need to make these configurable.
    domain = 'example.some.domain'
    email = 'example@example.com'
    # This gets set in the puppet config in /modules/mysql/files/mysql-server.preseed
    mysqlpass = 'root'
    fab_preseed_config(domain, email, mysqlpass)
    fab_install_debs(debs)
    fab_run_provision_tests()
    fab_apt_uninstall_aegir()
    
def aegir_developer_install(release_type = 'git', aegir_version = '6.x-1.x', drush_version = '7.x-5.7'):
    fab_prepare_apache()
    fab_prepare_user()
    fab_fetch_drush(drush_version)
    fab_link_provision()
    # Not sure if we need to make these configurable.
    domain = 'example.some.domain'
    email = 'example@example.com'
    # This gets set in the puppet config in /modules/mysql/files/mysql-server.preseed
    mysqlpass = 'root'
    fab_hostmaster_local_install(domain, email, mysqlpass)
    #fab_run_provision_tests()

<?php

/**
 * Implements hook_vagrant_blueprints().
 */
function aegir_developer_vagrant_blueprints() {
  $blueprints = array(
    'aegir_developer' => array(
      'name' => dt('Aegir developer 1.x'),
      'description' => dt('The default blueprint.'),
      'path' => 'blueprints/2.x',
      'build_callback' => 'aegir_developer_build',
    ),
  );
  return $blueprints;
}


/**
 * Implemention of COMMANDFILE_EXTENSION_build().
 */
function aegir_developer_build() {
  aegir_developer_build_steps(vagrant_default_build_vars());
}

function aegir_developer_build_steps($build) {
  aegir_developer_build_step_clone_projects($build);
  vagrant_default_build_project_dir($build);
  vagrant_default_build_project_setup($build);
  vagrant_default_build_config_dir($build);
  vagrant_default_build_record_blueprint($build);
  vagrant_default_build_user_data($build);
  vagrant_default_build_user_dotfiles($build);
  vagrant_default_build_git_init($build);
  aegir_developer_build_step_copy_projects($build);
  aegir_developer_build_step_make_hostmaster($build);
  aegir_developer_build_step_symlink_apt_cache($build);
}

function aegir_developer_build_step_clone_projects($build) {
  // Get our target directory we'll clone to
  $target = dirname(__FILE__) . '/lib/provision';
  // Clone Provision into it
  if (!is_dir($target)) {
    drush_shell_exec_interactive("git clone --recursive http://git.drupal.org/project/provision.git $target");
  }
  // Get our target directory we'll clone to
  $target = dirname(__FILE__) . '/lib/hostmaster';
  // Clone Hostmaster into it
  if (!is_dir($target)) {
    drush_shell_exec_interactive("git clone --recursive http://git.drupal.org/project/hostmaster.git $target");
  }
}

function aegir_developer_build_step_copy_projects($build) {
  // Get our extension's directory
  $source = dirname(__FILE__) . '/lib/provision';
  $target = $build['project_path'] . '/provision';
  // Copy Drupal from our dir to the project dir
  if (is_dir($source) && is_dir($build['project_path'])) {
    drush_copy_dir($source, $target);
    //TODO: Make this branch configurable.
    drush_shell_cd_and_exec($target, 'git checkout 6.x-2.x');
  }
  
  $source = dirname(__FILE__) . '/lib/hostmaster';
  $target = $build['project_path'] . '/hostmaster';
  // Copy Drupal from our dir to the project dir
  if (is_dir($source) && is_dir($build['project_path'])) {
    drush_copy_dir($source, $target);
    // This still needs to be 'made' using Drush make.
    //TODO: Make this branch configurable.
    drush_shell_cd_and_exec($target, 'git checkout 6.x-2.x');
  }
}

function aegir_developer_build_step_make_hostmaster($build) {
  $directory = $build['project_path'] . '/hostmaster';
  $profile = 'hostmaster';
  
  $args = array();
  $options = drush_redispatch_get_options();
  unset($options['no-working-copy']);

  $args[] = $directory . '/' . $profile . '.make';
  $args[] = '.';

  // We cd to run the command, so the contrib destination should be set to: '.'
  $options['contrib-destination'] = '.';

  // People should use drush make to make with a valid Drupal core.
  $options['no-core'] = TRUE;

  // Set the working-copy option by default, but allow people to not send it.
  if (drush_get_option('no-working-copy', FALSE)) {
    $options['working-copy'] = FALSE;
  }
  else {
    $options['working-copy'] = TRUE;
  }

  // Don't run with a Drupal root.
  $options['root'] = NULL;

  // Call the command
  $backend_options = array(
    'interactive' => TRUE,
  );
  
  
  $running_path = getcwd();
  // Convert windows paths.
  $running_path = _drush_convert_path($running_path);

  // Set to the path we want to use.
  chdir($directory);

  drush_invoke_process('@none', 'make', $args, $options, $backend_options);

  // Set back to the previous running path.
  chdir($running_path);
}

function aegir_developer_build_step_symlink_apt_cache($build) {
  // Get our extension's directory
  $source = dirname(__FILE__) . '/lib/tmp';
  $target = $build['project_path'] . '/tmp';
  drush_delete_dir($target);
  drush_shell_exec("ln -s '%s/' '%s'", $source, $target);
}